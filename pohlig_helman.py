import random
from primes import is_prime
from integers import egcd

PRIME_SIZE = 19


def get_random_prime():
    while True:
        i = random.randrange(2**PRIME_SIZE, 2**(PRIME_SIZE + 5))
        if is_prime(i):
            return i


def get_random_e_d(p):
    """ where (e, p-1) = 1"""
    while True:
        e = random.randrange(2**PRIME_SIZE, p)
        r, s, t = egcd(e, p-1)
        if r == 1:
            return e, s % (p-1)


def main():
    p = get_random_prime()
    print("p =", p)
    e, d = get_random_e_d(p)
    print("e =", e)
    print("d =", d)

    message = int.from_bytes(b'as', byteorder='little')
    assert message < p

    print("Message:", message)
    cipher = pow(message, e, p)
    print("Cipher:", cipher)
    decoded = pow(cipher, d, p)
    print("Decoded:", decoded)
    print("Decrypted:", decoded.to_bytes((decoded.bit_length() + 7) // 8, byteorder='little'))


if __name__ == '__main__':
    main()
