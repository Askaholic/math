def factorial_mod_print(n, m):
    print_count = 0
    ans = n
    for i in range(2, n):
        ans *= i
        ans %= m
        if print_count == 5000:
            print(" "*20, end="\r")
            print("{1:.2f}% - {0:}".format(ans, i/m * 100), end="\r")
            print_count = 0
        if ans == 0:
            return ans
        print_count += 1
    return ans


def is_prime_print(n):
    return factorial_mod_print(n - 1, n) - n == -1


def factorial_mod(n, m):
    ans = n
    for i in range(2, n):
        ans = (ans * i) % m
        if ans == 0:
            return 0
    return ans


def is_prime(n):
    return factorial_mod(n - 1, n) - n == -1
