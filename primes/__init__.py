from .wilson import is_prime
from itertools import tee


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def primes(max, prime_fn=is_prime):
    return filter(prime_fn, range(2, max))


def twin_primes(max, prime_fn=is_prime):
    return filter(
        lambda x: x[1] - x[0] == 2,
        pairwise(primes(max, prime_fn))
    )
