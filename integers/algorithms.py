import math
from typing import Optional, Tuple


def gcd(a: int, b: int) -> int:
    """ Euclidian algorithm """
    while b != 0:
        a, b = b, a % b
    return a


def egcd(a: int, b: int) -> Tuple[int, int, int]:
    """ Extended euclidian algorithm """
    s0, s1 = 1, 0
    t0, t1 = 0, 1
    r0, r1 = a, b
    while r1 != 0:
        q = r0 // r1
        r0, r1 = r1, r0 - q * r1
        s0, s1 = s1, s0 - q * s1
        t0, t1 = t1, t0 - q * t1
    return r0, s0, t0


def mod_inverse(a: int, n: int):
    """ Compute a modular inverse using the extended euclidian algorithm """
    return egcd(a, n)[1] % n


def babystep_giantstep(b: int, a: int, n: int) -> Optional[int]:
    """ Compute a discrete logarithm x such that pow(a, x, n) == b using the
    baby-step giant-step algorithm. This is an improvement on the brute force
    approach, trading extra memory requirements for speed.

    # Time Complexity
    O(sqrt(n))

    # Space Complexity
    O(sqrt(n))

    https://en.wikipedia.org/wiki/Baby-step_giant-step
    """
    m = int(math.sqrt(n)) + 1
    lookup = {}
    j_ = 1
    for j in range(m+1):
        lookup[j_] = j
        j_ *= a
        j_ %= n
    a_inv = mod_inverse(a, n)

    if a_inv * a % n != 1:
        # No inverse exists
        return None
    factor = pow(a_inv, m, n)

    g = b
    for i in range(m):
        j = lookup.get(g)
        if j:
            return i * m + j
        g = (g * factor) % n
    return None


def discrete_log_brute_force(b: int, a: int, n: int) -> Optional[int]:
    """ Compute a discrete logarithm x such that pow(a, x, n) == b by brute force.

        This will be extremely slow, but can be used to check a small amount of
        numbers mod a large n.

        # Time Complexity
        O(n)
    """
    for i in range(n):
        x = pow(a, i, n)
        if x == b:
            return i
    return None
