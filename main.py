from primes import twin_primes
from primes.wilson import is_prime_print


def main():
    # print(list(twin_primes(20000)))
    n = 101010293
    print("{} is {}prime".format(n, "" if is_prime_print(n) else "not "))


if __name__ == '__main__':
    main()
